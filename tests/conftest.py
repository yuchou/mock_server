import pytest
from mock.mockserver import MockServer


@pytest.fixture
def mock_server(request):
    server = MockServer()
    server.start()
    yield server
    server.shutdown_server()