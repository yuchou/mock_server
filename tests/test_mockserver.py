import pytest
import requests
from urllib.parse import urljoin


def test_mock_with_json_serializable(mock_server):
    api_path = "/json"
    url = urljoin(mock_server.url, api_path)
    mock_server.add_json_response(api_path, dict(hello="welt"))
    response = requests.get(url)

    assert 200 == response.status_code
    assert 'hello'in response.json()
    assert 'welt' == response.json()['hello']


def test_mock_with_callback(mock_server):
    called = False
    api_path = "/callback"
    url = urljoin(mock_server.url, api_path)

    def callback():
        called = True
        return 'Hallo'

    mock_server.add_callback_response(api_path, callback)
    response = requests.get(url)

    assert 200 == response.status_code
    assert 'Hallo' == response.text
    assert called
